import python_version

try:
    python_version.check(min=(3, 0, 0), max=(4, 0, 0))
except Exception as e:
    print(repr(e))
else:
    print("All good!")

try:
    python_version.check(min=(3, 6, 0), max=(4, 0, 0))
except Exception as e:
    print(repr(e))
else:
    print("All good!")

try:
    python_version.check(min=(2, 7, 0), max=(2, 7, 999))
except Exception as e:
    print(repr(e))
else:
    print("All good!")

try:
    python_version.check(min=(2, 7, 0), max=(2, 7, 999), exit_on_error=True)
except Exception as e:
    print(repr(e))
else:
    print("All good!")
